package com.system.service;

import com.system.common.vo.emp.EmpQuery;
import com.system.pojo.Dept;
import com.system.pojo.Emp;

import java.util.List;

/**
 * @Company
 * @Interfacename EmpService
 * @Description TODO
 * @Author pn
 * Date 2021-08-15 17:16
 * Version 1.0
 */
public interface EmpService {

    List<Emp> getEmpList(EmpQuery param);

    Long countEmpList(EmpQuery param);

    void addEmp(Emp emp);

    List<Dept> getAllDept();

    void deleteEmpByIds(String ids);

    Emp getEmpById(Integer id);

    void updateEmp(Emp emp);
}
