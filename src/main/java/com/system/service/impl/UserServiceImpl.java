package com.system.service.impl;

import com.system.mapper.UserMapper;
import com.system.pojo.User;
import com.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Company
 * @Classname UserServiceImpl
 * @Description TODO
 * @Author pn
 * Date 2021-08-14 12:47
 * Version 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public User login(User user) {
        return userMapper.getUser(user);
    }
}
