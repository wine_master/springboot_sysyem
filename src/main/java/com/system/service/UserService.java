package com.system.service;

import com.system.pojo.User;
import org.springframework.stereotype.Service;

/**
 * @Company
 * @Interfacename UserService
 * @Description TODO
 * @Author pn
 * Date 2021-08-14 12:46
 * Version 1.0
 */
public interface UserService {
    User login(User user);
}
