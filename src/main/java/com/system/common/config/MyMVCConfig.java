package com.system.common.config;

import com.system.common.intercepter.LoginHandlerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Company
 * @Classname MyMVCConfig
 * @Description TODO
 * @Author pn
 * Date 2021-08-14 21:15
 * Version 1.0
 */
@Configuration
public class MyMVCConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/index.html").setViewName("index");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginHandlerInterceptor())
                .addPathPatterns("/**") //所有请求都拦截,拦截请求
                .excludePathPatterns(
                        "/",
                        "/user/login",
                        "/captcha",
                        "/login",

                        //静态资源
                        "/api/**",
                        "/css/**",
                        "/images/**",
                        "/js/**",
                        "/layui/**",
                        "/lib/**",
                        "/webjars/**");//除了这些不拦截，放行请求
    }
}
