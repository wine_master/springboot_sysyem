package com.system.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Company
 * @Classname Result
 * @Description TODO
 * @Author pn
 * Date 2021-08-14 12:57
 * Version 1.0
 */
@Data
public class Result<T> {
    private Integer code; //返回码， 0成功
    private String message; //返回描述
    private T data; //返回数据
    private Long count; //layUi的总查询记录数

    public Result() {
    }

    public Result(Integer code, String message, T data, Long count) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.count = count;
    }

    public static Result<Object> success(){
        return new Result(0,"success",null,null);
    }

    public static Result<Object> success(String message){
        return new Result(0,message,null,null);
    }

    public static Result<Object> success(Object data,Long count){
        return new Result(0,"success",data,count);
    }

    public static Result<Object> fail(){
        return new Result(-1,"success",null,null);
    }

    public static Result<Object> fail(String message){
        return new Result(-1,message,null,null);
    }
}
