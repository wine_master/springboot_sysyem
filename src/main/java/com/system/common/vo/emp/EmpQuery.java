package com.system.common.vo.emp;

import com.system.common.vo.Page;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Company
 * @Classname EmpQuery
 * @Description TODO
 * @Author pn
 * Date 2021-08-15 17:04
 * Version 1.0
 */
@Data
public class EmpQuery extends Page {
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
