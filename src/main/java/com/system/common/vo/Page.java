package com.system.common.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Company
 * @Classname Page
 * @Description TODO
 * @Author pn
 * Date 2021-08-15 17:03
 * Version 1.0
 */

@Data
public class Page implements Serializable {
    private Integer page;
    private Integer limit;

    public Long getStart(){
        return (page - 1L) * limit;
    }
}
