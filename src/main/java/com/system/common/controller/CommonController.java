package com.system.common.controller;

import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Company
 * @Classname CommonController
 * @Description TODO
 * @Author pn
 * Date 2021-08-14 11:57
 * Version 1.0
 */
@Controller
public class CommonController {
    @GetMapping("/hello")
    @ResponseBody
    public String test(){
        return "hello Spring";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    //欢迎也=页
    @RequestMapping("/welcome")
    public String welcome(){
        return "welcome";
    }

    //验证码
    @RequestMapping("/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CaptchaUtil.out(request, response);
    }
}
