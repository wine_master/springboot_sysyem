package com.system.mapper;

import com.system.pojo.Dept;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Company
 * @Interfacename DeptMapper
 * @Description TODO
 * @Author pn
 * Date 2021-08-16 17:54
 * Version 1.0
 */
@Repository
public interface DeptMapper {

    List<Dept> getAllDept();
}
