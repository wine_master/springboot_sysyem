package com.system.mapper;

import com.system.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Company
 * @Interfacename UserMapper
 * @Description TODO
 * @Author pn
 * Date 2021-08-14 12:37
 * Version 1.0
 */
/*@Mapper *///在启动里是@MapperScan一样
@Repository //交给spring托管
public interface UserMapper {
    User getUser(User user);
}
