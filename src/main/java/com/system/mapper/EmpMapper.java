package com.system.mapper;

import com.system.common.vo.emp.EmpQuery;
import com.system.pojo.Emp;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Company
 * @Interfacename EmpMapper
 * @Description TODO
 * @Author pn
 * Date 2021-08-15 17:28
 * Version 1.0
 */
/*@Mapper*/
@Repository
public interface EmpMapper {

    List<Emp> getEmpList(EmpQuery param);

    Long countEmpList(EmpQuery param);

    void addEmp(Emp emp);

    void deleteEmpByIds(String ids);

    Emp getEmpById(Integer id);

    void updateEmp(Emp emp);
}
