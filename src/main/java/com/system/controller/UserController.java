package com.system.controller;

import com.system.common.vo.Result;
import com.system.pojo.User;
import com.system.service.UserService;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Company
 * @Classname UserController
 * @Description TODO
 * @Author pn
 * Date 2021-08-14 13:18
 * Version 1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/login")
    public Result login(User param, @RequestParam("captcha") String captcha, HttpServletRequest request, HttpSession session){

        if (!CaptchaUtil.ver(captcha, request)) {
            CaptchaUtil.clear(request);  // 清除session中的验证码
            return Result.fail("验证码不正确");
        }
        User user = userService.login(param);
        System.out.println("======>" + user);

        //验证
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(); //md5有言加密
        //param.getPassword()为前端的参数，user.getPassword()为数据库的值
        boolean matches = encoder.matches(param.getPassword(), user.getPassword());

        if (matches){
            System.out.println(param.getPassword());
            System.out.println(user.getPassword());
            //登录成功
            //存入session
            user.setPassword("==========");
            session.setAttribute("userInfo",user);
            System.out.println(user.getPassword());
            return Result.success();
        }else {
            //登录失败
            return Result.fail("用户名或密码错误！");
        }
    }
}
