package com.system;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.DigestUtils;

@SpringBootTest
class SpringbootSysyemApplicationTests {

    @Test
    void contextLoads() {
        //md5 spring提供的加密方法，加盐得自己处理
        String s1 = DigestUtils.md5DigestAsHex("123456".getBytes());
        String s2 = DigestUtils.md5DigestAsHex("123456".getBytes());

        System.out.println(s1);
        System.out.println(s2);

        // spring安全框架提供的加密方法，可以自动加盐，无需自己保存盐值
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode1 = encoder.encode("123456");
        String encode2 = encoder.encode("123456");

        System.out.println(encode1);
        System.out.println(encode2);

        //验证
        boolean b = encoder.matches("123456", "$2a$10$/FZz42u4gl2fxUo/NKDEzemUB.U1MrBxxWZSITuXNTph1fy5gCYoa");
        boolean b1 = encoder.matches("123456", "$2a$10$a1mV.0koXNxrI1q58JvOzuXbrC8lHqbArzBGb48okt2yny18djLgS");

        System.out.println(b);
        System.out.println(b1);
    }

}
